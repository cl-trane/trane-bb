;; -*- lisp -*-

;;;; Copyright (c) 2008, Maciej Pasternacki <maciekp@japhy.fnord.org>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(in-package :cl-user)

(defpackage #:trane-bb.asd
  (:use #:cl #:asdf))

(in-package #:trane-bb.asd)

(asdf:defsystem #:trane-bb
  :name "BB"
  :description "Parser for BBCode."
  :version "0.1"
  :author "Maciej Pasternacki <maciekp@japhy.fnord.org>"
  :licence "BSD sans advertising clause (see file COPYING for details)"
  :components ((:file "bb"))
  :depends-on (#:meta-sexp))
