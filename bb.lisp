;;; bb.lisp - BBCode parser

;;;; Copyright (c) 2008, Maciej Pasternacki <maciekp@japhy.fnord.org>
;;;; All rights reserved.  This file is available on the terms
;;;; detailed in COPYING file included with it.

(defpackage #:trane-bb
  (:use #:common-lisp #:meta-sexp)
  (:export #:bb))
(in-package #:trane-bb)

(defun html-quote-char (char stream)
  "Output CHAR safely escaped for HTML to STREAM."
  (case char
    (#\< (write-string "&lt;" stream))
    (#\> (write-string "&gt;" stream))
    (#\& (write-string "&amp;" stream))
    (#\" (write-string "&quot;" stream))
    (t (write-char char stream))))

(defun html-quote-string (string)
  "Escape STRING for HTML."
  (with-output-to-string (s)
    (map nil #'(lambda (c) (html-quote-char c s)) string)))

(declaim (ftype function bb-inline))

;;; inline, <tag>interior</tag>
(defrenderer bb-simple! (tag arg interior &aux (lctag (string-downcase (string tag))))
    (attachment)
  (format attachment "<~A>~A</~A>" lctag (bb-inline interior) lctag))

;;; inline, for [url]
(defrenderer bb-url! (tag arg interior) (attachment)
  (format attachment "<a href=\"~A\">~A</a>"
          (if (string= "" arg)
              interior
              arg)
          (bb-inline interior)))

;;; inline, for [color]
(defrenderer bb-color! (tag arg interior) (attachment)
  (format attachment "<span style=\"color:~A;\">~A</span>"
          (html-quote-string arg) (bb-inline interior)))

;;; inline, sets style=TAG
(defrenderer bb-style! (tag arg interior) (attachment)
  (format attachment "<span style=\"~A\">~A</span>"
          tag (bb-inline interior)))

;;; inline, [size]
(defrenderer bb-size! (tag arg interior) (attachment)
  (format attachment "<span style=\"font-weight:~[xx-small~;x-small~;small~;medium~;large~;x-large~;xx-large~];\">~A</span>"
          (+ 3 (parse-integer arg)) (bb-inline interior)))

;;; block, [quote]
(defrenderer bb-quote! (tag arg interior) (attachment)
  (format attachment "<blockquote>~:[<q>~A</q>~;~A~]~A</blockquote>"
          (string= "" arg)
          (html-quote-string arg)
          (bb-inline interior)))

;;; Helper
(defrule whitespace*? () ()
  (:* (:or (:type white-space?)
           (:type newline?))))

;;; list item, inline, meaningful only within [list] block
(defrule bb-list-item? (&aux (interior (make-char-accum))) ()
  (:rule whitespace*?)
  "[*]" (:* (:not (:checkpoint "[*]"))
            (:char-push interior))
  (:render bb-simple! "li" nil interior))

;;; list interior, many list items
(defrule bb-list-interior? () ()
  (:+ (:rule bb-list-item?)))

;;; list
(defrenderer bb-list! (tag arg interior) (attachment)
  (setf interior (with-output-to-string (s)
                   (bb-list-interior? (create-parser-context interior :attachment s))))
  (if (string= "" arg)
      (format attachment "<ul>~A</ul>" interior)
      (format attachment "<ol style=\"list-style-type:~A;\">~A</ol>"
              (cond ((string= arg "1") "decimal")
                    ((string= arg "01") "decimal-leading-zero")
                    ((string= arg "i") "lower-roman")
                    ((string= arg "I") "upper-roman")
                    ((string= arg "a") "lower-latin")
                    ((string= arg "A") "upper-latin")
                    ((string= arg "alpha") "lower-greek")
                    ((string= arg "α") "lower-greek")
                    (t (error "Unsupported enumeration style.")))
              interior)))


(defmacro defbbrule (bbtag renderer &optional (tag `(quote ,bbtag)))
  (let ((rule-name (intern (concatenate 'string
                                  (string '#:bb-tag-)
                                  (string bbtag)
                                  "?"))))
    `(defrule ,rule-name (&aux (arg (make-char-accum)) (interior (make-char-accum))) ()
       #\[ (:icase ,(string bbtag))
       (:? "=" (:+ (:not "]")
                   (:char-push arg)))
       #\]
       (:* (:not (:checkpoint #\[ #\/ (:icase ,(string bbtag)) #\]))
           (:char-push interior))
       (:? #\[ #\/ (:icase ,(string bbtag)) #\])
       ,(if (atom renderer)
            `(:render ,renderer ,tag arg interior)
            (cons :render renderer)))))

;;; Inline tags
(defbbrule b bb-simple!)
(defbbrule i bb-simple!)
(defbbrule u bb-style! "font-decoration: underline;")
(defbbrule s bb-style! "font-decoration: line-through;")
(defbbrule url bb-url!)
(defbbrule color bb-color!)
(defbbrule size bb-size!)

;;; Block-level tags
(defbbrule quote bb-quote!)
(defbbrule list bb-list!)

(defrule bb-inline? (&aux c) (attachment)
  (:* (:or (:rule bb-tag-b?)
           (:rule bb-tag-i?)
           (:rule bb-tag-u?)
           (:rule bb-tag-s?)
           (:rule bb-tag-color?)
           (:rule bb-tag-size?)
           (:rule bb-tag-url?)
           (:and (:assign c (:read-atom))
                 (html-quote-char c attachment)))))

(defun bb-inline (text)
  (with-output-to-string (s)
    (bb-inline? (create-parser-context text :attachment s))))

(defrenderer bb-paragraph! (text) (attachment)
  (format attachment "<p>~A</p>" (bb-inline text)))

(defrule bb-paragraph? (&aux (text (make-char-accum))) ()
  (:+ (:not (:checkpoint (:type newline?)
                         (:type newline?)))
      (:char-push text))
  (:? (:type newline?)
      (:type newline?))
  (:render bb-paragraph! text))

(defrule bb-document? () ()
  (:* (:rule whitespace*?)
      (:not (:eof))
      (:or (:rule bb-tag-quote?)
           (:rule bb-tag-list?)
           (:rule bb-paragraph?))))

(defun bb (input &optional stream)
  "Parse INPUT (either string or input stream) as BBCode, format
resulting HTML to STREAM or return it as string if STREAM is not
given or NIL. "
  (if stream
      (bb-document? (create-parser-context input :attachment stream))
      (with-output-to-string (s)
        (bb-document? (create-parser-context input :attachment s)))))
